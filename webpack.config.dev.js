var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  devtool: 'source-map',
  entry: './app/index',
  output: {
    path: path.join(__dirname, 'app'),
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      {test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/,         // Need this here for prosemirror til it has own .babelrc
        query: {
          presets: ['es2015']
        }},
      {test: /\.css$/, loader: 'style!css'},
      // {
      //   test: /\.incl.html$/,
      //   loader: 'ng-cache?-url',
      //   exclude: /node_modules/
      // },
      {
        test: /\.html$/,
        loader: 'raw',
        exclude: [/node_modules/, /\.incl.html$/]
      },
      {
        test: /\.(eot|woff|woff2|ttf|svg|png|jpe?g|gif)(\?\S*)?$/,
        // test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)$/,
        loader: 'url-loader?limit=10000'
        // exclude: /node_modules/
      },
      {
        test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)$/,
        loader: 'file?name=[path][name].[ext]?[hash]'
        // exclude: /node_modules/
      },
      {test: /\.json$/, loader: 'json'}
    ]
  },
  plugins: [
    new webpack.ContextReplacementPlugin(/moment[\\\/]locale$/, /^\.\/(en)$/),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './app/index.html',
      hash: true
    })
  ]
};