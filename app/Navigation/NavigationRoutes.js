export default function NavigationRoutes($stateProvider) {
  $stateProvider
    .state({
      name: 'home',
      url: '/test',
      template: '<home/>'
    })
}

NavigationRoutes.$inject = [ '$stateProvider' ]