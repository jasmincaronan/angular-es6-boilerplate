import angular from 'angular'
import 'angular-ui-router'
import NavigationRoutes from './NavigationRoutes'
import NavigationComponent from './NavigationComponent'

angular.module('NavigationModule', ['ui.router'])
  .component('home', NavigationComponent)
  .config(NavigationRoutes)
