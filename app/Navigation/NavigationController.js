export class NavigationController {
  constructor(t) {
    this.name = "Developer"
    t(() => console.log("Here's a delayed 'Hello, Developer"), 2500)
  }
}

NavigationController.$inject = [ '$timeout' ]
