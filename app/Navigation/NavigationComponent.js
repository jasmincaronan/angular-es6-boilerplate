import navTemplate from './nav.template.html'
import {NavigationController} from './NavigationController'
export default {
	
	template: navTemplate,
	controller: NavigationController 	
}

// const AppComponent = {
	
// 	template: nav_template,
// 	controller: NavigationController 	
// }

// export default AppComponent;