import dashboardTemplate from './dashboard.template.html'
import {DashboardController} from './DashboardController'
export default {
	
	template: dashboardTemplate,
	controller: DashboardController 	
}
