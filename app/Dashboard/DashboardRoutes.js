export default function DashboardRoutes($stateProvider) {
  $stateProvider
    .state({
      name: 'dashboard',
      url: '',
      template: '<dashboard/>'
    })
}

DashboardRoutes.$inject = [ '$stateProvider' ]