import angular from 'angular'
import 'angular-ui-router'
import DashboardRoutes from './DashboardRoutes'
import DashboardComponent from './DashboardComponent'
import RdLoading from './Directives/RdLoading'
import RdWidgetBody from './Directives/RdWidgetBody'
import RdWidgetFooter from './Directives/RdWidgetFooter'
import RdWidgetTitle from './Directives/RdWidgetTitle'
import RdWidget from './Directives/RdWidget'

angular.module('DashboardModule', ['ui.router'])
  .component('dashboard', DashboardComponent)
  .config(DashboardRoutes)
  .directive('rdLoading', RdLoading)
  .directive('rdWidgetBody', RdWidgetBody)
  .directive('rdWidgetFooter', RdWidgetFooter)
  .directive('rdWidgetHeader', RdWidgetTitle)
  .directive('rdWidget', RdWidget)
