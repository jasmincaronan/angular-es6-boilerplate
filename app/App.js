import angular from 'angular'
import './Navigation/NavigationModule'
import './Dashboard/DashboardModule'
import 'angular-bootstrap'
import './../node_modules/bootstrap/dist/css/bootstrap.min.css'
import './../bower_components/rdash-ui/dist/css/rdash.min.css'
import './../node_modules/font-awesome/css/font-awesome.min.css'

angular.module('App', [
	'ui.bootstrap',
	'NavigationModule',
	'DashboardModule'
	])